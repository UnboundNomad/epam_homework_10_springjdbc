package sql.dao;

import org.springframework.stereotype.Repository;
import sql.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

@Repository
public class UserDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<User> ROW_MAPPER = (rs, rowNum) -> new User(rs.getInt("user_id"),
            rs.getString("full_name"),
            rs.getString("billing_adress"),
            rs.getString("login"),
            rs.getString("password"));


    public List<User> getAllUsers() {
        String sql = "SELECT * FROM users ";
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    public void addUser(User user) {
        String sql = "INSERT INTO users (full_name, billing_adress, login, password) " +
                "VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, user.getFull_name(), user.getBilling_address(), user.getLogin(), user.getPassword());
    }
}

