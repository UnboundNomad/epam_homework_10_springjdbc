package sql.dao;

import org.springframework.stereotype.Repository;
import sql.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ItemDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Item> ROW_MAPPER = (rs, rowNum) -> new Item(rs.getInt("item_id"),
            rs.getString("title"),
            rs.getString("description"),
            rs.getDouble("start_price"),
            rs.getDouble("bid_increment"),
            rs.getDate("start_date").toLocalDate(),
            rs.getDate("stop_date").toLocalDate(),
            rs.getInt("users_user_id"));


    public List<Item> getAllItems() {
        String sql = "SELECT * FROM items";
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    public List<Item> getItemsByUserId(int userId) {
        String sql = "SELECT * FROM items WHERE users_user_id =?";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    public List<Item> searchItemsByName(String search) {
        String sql = "SELECT * FROM items WHERE title like ?";
        search = "%" + search + "%";
        return jdbcTemplate.query(sql, new Object[]{search}, ROW_MAPPER);
    }

    public List<Item> searchItemsByDescription(String search) {
        String sql = "SELECT * FROM items WHERE description like ?";
        search = "%" + search + "%";
        return jdbcTemplate.query(sql, new Object[]{search}, ROW_MAPPER);
    }

    public List<Item> getActiveItemsByUserId(int userId) {
        String sql = "SELECT * FROM items " +
                "WHERE users_user_id = ? AND CURRENT_DATE() BETWEEN start_date AND stop_date";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    public void addItem(Item item) {
        String sql = "INSERT INTO items (title, description, start_price, bid_increment, start_date,stop_date, users_user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";
        jdbcTemplate.update(sql, item.getTitle(),
                item.getDescription(),
                item.getStart_price(),
                item.getBid_increment(),
                Date.valueOf(item.getStart_date()),
                Date.valueOf(item.getStart_date()),
                item.getUser_id());
    }

    public void deleteItemsByUserId(int userId) {
        String sql;
        sql = "DELETE bids FROM bids INNER JOIN items ON bids.items_item_id = items.item_id " +
                "WHERE items.users_user_id = ?";
        jdbcTemplate.update(sql, userId);

        sql = "DELETE FROM items WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }

    public List<Map<String, Object>> getAveragePriceByUsers() {
        String sql = "SELECT full_name, AVG(start_price) " +
                "FROM items i INNER JOIN users u ON i.users_user_id = u.user_id " +
                "GROUP BY users_user_id";
        return jdbcTemplate.queryForList(sql);
    }

    public void doubleStartBidsByUserId(int userId) {
        String sql = "UPDATE items " +
                "SET start_price = start_price*2 " +
                "WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }
}
