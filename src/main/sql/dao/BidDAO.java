package sql.dao;

import org.springframework.stereotype.Repository;
import sql.dto.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import java.util.List;
import java.util.Map;

@Repository
public class BidDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Bid> ROW_MAPPER = (rs, rowNum) -> new Bid(rs.getInt("bid_id"),
            rs.getDate("bid_date").toLocalDate(),
            rs.getInt("bid_value"),
            rs.getInt("items_item_id"),
            rs.getInt("users_user_id"));


    public List<Bid> getAllBids() {
        String sql = "SELECT * FROM bids ";
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    public List<Bid> getBidsByUserId(int userId) {
        String sql = "SELECT * FROM bids WHERE users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{userId}, ROW_MAPPER);
    }

    public void deleteBidsByUserId(int userId) {
        String sql = "DELETE FROM bids WHERE users_user_id = ?";
        jdbcTemplate.update(sql, userId);
    }

    public List<Map<String, Object>> getMaxBidByItems() {
        String sql = "SELECT title, MAX(bid_value) " +
                "FROM items i INNER JOIN bids b ON i.item_id = b.items_item_id " +
                "GROUP BY title";
        return jdbcTemplate.queryForList(sql);
    }
}
