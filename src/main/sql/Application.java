package sql;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sql.config.AppConfig;
import sql.demo.IDemoService;

public class Application {

	public static void main(String[] args) {
		new AnnotationConfigApplicationContext(AppConfig.class).getBean(IDemoService.class).execute();
	}

}

