package sql.dto;

import java.time.LocalDate;

public class Bid {

    private int id;
    private LocalDate bid_date;
    private int bid_value;
    private int item_id;
    private int user_id;

    public Bid(int id, LocalDate bid_date, int bid_value, int item_id, int user_id) {
        this.id = id;
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.item_id = item_id;
        this.user_id = user_id;
    }

    public Bid(LocalDate bid_date, int bid_value, int item_id, int user_id) {
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.item_id = item_id;
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return String.format("id= %d; bid_date= %s; bid_value= %s; item_id= %s; user_id= %s", id, bid_date, bid_value, item_id, user_id);
    }
}
