package sql.dto;

public class User {

    private int id;
    private String full_name;
    private String billing_address;
    private String login;
    private String password;

    public User(String full_name, String billing_address, String login, String password) {
        this.full_name = full_name;
        this.billing_address = billing_address;
        this.login = login;
        this.password = password;
    }

    public User(int id, String full_name, String billing_address, String login, String password) {
        this.id = id;
        this.full_name = full_name;
        this.billing_address = billing_address;
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("id= %d; full_name= %s; billing_address= %s; login= %s", id, full_name,billing_address,login);
    }

    public String getFull_name() {
        return full_name;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
