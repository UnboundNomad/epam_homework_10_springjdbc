package sql.dto;

import java.time.LocalDate;

public class Item {

    private int id;
    private String title;
    private String description;
    private double start_price;
    private double bid_increment;
    private LocalDate start_date;
    private LocalDate stop_date;
    private int user_id;

    public Item(String title, String description, double start_price, double bid_increment, LocalDate start_date, LocalDate stop_date, int user_id) {
        this.title = title;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.user_id = user_id;
    }

    public Item(int id, String title, String description, double start_price, double bid_increment, LocalDate start_date, LocalDate stop_date, int user_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return String.format("id= %d; title= %s; description= %s; start_price= %f; bid_increment= %f; start_date= %s; stop_date= %s; user_id= %s",
                id, title, description, start_price, bid_increment, start_date, stop_date, user_id);
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getStart_price() {
        return start_price;
    }

    public double getBid_increment() {
        return bid_increment;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public LocalDate getStop_date() {
        return stop_date;
    }

    public int getUser_id() {
        return user_id;
    }
}
