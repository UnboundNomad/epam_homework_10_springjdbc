package sql.demo;


import sql.dto.Item;
import sql.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sql.dao.BidDAO;
import sql.dao.ItemDAO;
import sql.dao.UserDAO;

import java.time.LocalDate;

@Service
public class DemoService implements sql.demo.IDemoService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private ItemDAO itemDAO;

    @Autowired
    private BidDAO bidDAO;


    public void execute() {

        System.out.println("\n1. Ставки пользователя с id = 3:");
        bidDAO.getBidsByUserId(3).forEach(System.out::println);

        System.out.println("\n2. Лоты пользователя с id = 4:");
        itemDAO.getItemsByUserId(4).forEach(System.out::println);

        System.out.println("\n3. Лоты содержащие в названии \"зев\":");
        itemDAO.searchItemsByName("зев").forEach(System.out::println);

        System.out.println("\n4. Лоты содержащие в описании \"посох\":");
        itemDAO.searchItemsByDescription("посох").forEach(System.out::println);

        System.out.println("\n5. Средняя цена лотов каждого пользователя:");
        itemDAO.getAveragePriceByUsers().forEach(System.out::println);

        System.out.println("\n6. Максимальный размер имеющихся ставок на каждый лот:");
        bidDAO.getMaxBidByItems().forEach(System.out::println);

        System.out.println("\n7. Список действующих лотов пользователя с id = 3:");
        itemDAO.getActiveItemsByUserId(3).forEach(System.out::println);

        System.out.println("\n8. Добавить нового пользователя");
        userDAO.addUser(new User("Русов Антон",
                "г. Злынка, ул. Солянка, дом 7, квартира 82",
                "RusovAnton362",
                "uv5LKVchaPu6"));

        System.out.println("\n9. Добавить новый лот");
        itemDAO.addItem(new Item("Высвобожденная погремушка странника", "Высвобожденная погремушка странника",
                2300.0,
                150.0,
                LocalDate.of(2019, 1, 6),
                LocalDate.of(2019, 1, 14),
                10));

        System.out.println("\n10. Удалить ставки пользователя c id = 2");
        bidDAO.deleteBidsByUserId(3);

        System.out.println("\n11. Удалить лоты пользователя c id = 5");
        itemDAO.deleteItemsByUserId(5);

        System.out.println("\n12. Увеличить стартовые цены товаров пользователя c id = 6 вдвое");
        itemDAO.doubleStartBidsByUserId(6);

        System.out.println("\nВсе пользователи:");
        userDAO.getAllUsers().forEach(System.out::println);

        System.out.println("\nВсе лоты:");
        itemDAO.getAllItems().forEach(System.out::println);

        System.out.println("\nВсе ставки:");
        bidDAO.getAllBids().forEach(System.out::println);
    }
}


